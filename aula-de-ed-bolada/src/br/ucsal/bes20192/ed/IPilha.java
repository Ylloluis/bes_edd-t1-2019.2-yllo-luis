package br.ucsal.bes20192.ed;

public interface IPilha {

	void push(int valor);
	No pop();
	void imprimir();

}