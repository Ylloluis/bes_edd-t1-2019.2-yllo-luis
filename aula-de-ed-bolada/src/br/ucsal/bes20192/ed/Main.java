package br.ucsal.bes20192.ed;

public class Main {

	public static void main(String[] args) {
		Pilha pilha = new Pilha();
		
		// Inser��o da pilha inicial
		pilha.push(1);
		pilha.push(3);
		pilha.push(5);
		pilha.push(7);
		pilha.push(11);
		pilha.imprimir();
		
		// Uso do pop
		pilha.pop();
		pilha.imprimir();
		
		// Uso do pop p�s pop
		pilha.pop();
		pilha.pop();
		pilha.imprimir();
	}

}
