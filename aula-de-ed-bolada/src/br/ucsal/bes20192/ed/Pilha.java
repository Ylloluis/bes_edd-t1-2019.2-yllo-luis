package br.ucsal.bes20192.ed;

public class Pilha implements IPilha {

	No topo = null;

	@Override
	public void push(int valor) {
		No novo = new No();
		novo.valor = valor;
		novo.prox = topo;
		topo = novo;
	
	}

	@Override
	public No pop() {
		if (topo == null) {
			System.out.println("n preenchido");
		} else {
			No aux = topo;
			topo = topo.prox;
			return aux;
		}
		return null;
	}

	@Override
	public void imprimir() {
		if (topo == null) {
			System.out.println("n preenchido");
		} else {
			No aux = topo;
			while(aux!=null) {
				System.out.print(aux.valor + " ");
				aux=aux.prox;
			}
		}
		System.out.println("");

	}

}
