package br.ucsal.bes20192.ed;

public class Questao02 {

	public static void main(String[] args) {
		Pilha pilha = new Pilha();
		
		// Inser��o da pilha inicial
		pilha.push(1);
		pilha.push(3);
		pilha.push(5);
		pilha.push(7);
		pilha.push(11);
		pilha.imprimir();
		
		pilha = inverte(pilha); 
		
		pilha.imprimir();
	}
	
	public static Pilha inverte(Pilha pilha) {
		Pilha invertida = new Pilha();
		No aux = pilha.topo;	
		while (aux != null) {
			invertida.push(aux.valor);
			aux = aux.prox;
		}
		return invertida;
	}
	
}
