package ucsal.br20192.edd.pratica.pilha;

/*
 * Aux.prox = ant.prox - Marcelo Indio
 * 
 * Teste a n�vel de c�digo do assunto pilhas.
 */

public class Pilha {
	
	static String alvoPilha[] = new String[100];
	static Integer posicaoDaPilha = 0;
	
	/*
	 * Inser��o de dado em um vetor de String
	 */
	public static void inserePilha(String valor) {
		if(tamanhoPilha().equals(0)) { 
			System.out.println("Pilha vazia");
			alvoPilha[posicaoDaPilha] = valor;
			posicaoDaPilha++;
		} else { 
			alvoPilha[posicaoDaPilha] = valor;
			posicaoDaPilha++;
		}
	}
	
	/*
	 * Remo��o de dado em um vetor de String
	 */
	public static void removePilha() {
		if(tamanhoPilha().equals(-1)) { 
			System.out.println("Pilha vazia");
		} else { 
			alvoPilha[posicaoDaPilha] = null;
			posicaoDaPilha--;
		}		
	}
	
	/*
	 * Obten��o de tamanho da pilha
	 */
	public static Integer tamanhoPilha() {
		if(alvoPilha.length == 0) { 
			return -1;
		} else { 
			return posicaoDaPilha;
		}
	}
	
	/*
	 * Teste de print da pilha
	 */
	public static Integer imprimirPilha(Integer posInicio) { 
		System.out.println(alvoPilha[posInicio]);
		if(posInicio <= 0) { 
			return -1;
		} else { 
			return (imprimirPilha(posInicio - 1));
		}
	}
	
	/*
	 * Teste de print do ultimo elemento da pilha
	 */
	public static void imprimirUltimoDaPilha() {
		Integer cont = posicaoDaPilha;
		cont--;
		System.out.println(alvoPilha[cont]);
	}
}
