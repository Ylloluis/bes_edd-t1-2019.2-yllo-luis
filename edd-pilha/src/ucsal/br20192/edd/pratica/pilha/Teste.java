package ucsal.br20192.edd.pratica.pilha;

public class Teste {

	public static void main(String[] args) {
		executar();
	}

	private static void executar() {
		Pilha.inserePilha("Pizza");
		Pilha.inserePilha("Pastel");
		Pilha.inserePilha("X-tudo");
		Pilha.inserePilha("Picanha");
		Pilha.inserePilha("Salada");
		Pilha.inserePilha("Mcdonald's");
		Pilha.imprimirUltimoDaPilha();
		System.out.println(Pilha.posicaoDaPilha);
		Pilha.removePilha();
		Pilha.imprimirUltimoDaPilha();
		System.out.println(Pilha.posicaoDaPilha);
		Pilha.imprimirPilha(Pilha.posicaoDaPilha);
	}

}
