package ucsal.br.bes20192.edd.prova;

import java.util.Scanner;

public class Agenda  {
	/**
	 * Classe de operações 
	 */
	
	//FIXME Por favor alguém refina essa classe eu fiz ela morrendo.
	
	private static Scanner sc = new Scanner(System.in);
	
	static private final int NUMERO_VETOR = 5;
	static Contato[] contatos = new Contato[NUMERO_VETOR];
	
	public static void inserir(Contato contato) {
		for (int i = 0; i < contatos.length; i++) {
			if(contatos[i] == null) { 
				contatos[i] = contato;
				break;
			}
		}	
	}

//	private static void transferirContatos(Contato[] contatos_new) {
//		for (int i = 0; i < contatos.length; i++) {
//			contatos[i] = contatos_new[i]; 
//		}	
//		contatos = contatos_new;	
//	}


	public static void remover(String cpf) throws NullPointerException {
		for (int i = 0; i < contatos.length; i++) {
			if(contatos[i].getCpf().equals(cpf)) {
				contatos[i] = null;
				System.out.println("Contato apagado com sucesso");
				System.out.println();
				break;
			} else if(contatos[i] == null) { 
				throw new NullPointerException("Lista de contatos incompleta");
			}
		}		
	}

	
	public static Contato consultarPorCpf(String cpf) {
		for (int i = 0; i < contatos.length; i++) {
			if(contatos[i].getCpf().equals(cpf)) {
				return contatos[i];
			} 
		}
		return null;
	}

	
	public static Contato[] consultarPorBairro(String bairro) {
		Contato[] contatosbairro = new Contato[contatos.length];
		for (int i = 0; i < contatos.length; i++) {
			if(contatos[i].getBairro().equals(bairro)) {
				contatosbairro[i] = contatos[i];
			} 
		}
		return contatosbairro;
	}

	
	public static Contato[] consultarPorCidade(String cidade) {
		for (int i = 0; i < contatos.length; i++) {
			if (contatos[i].getCidade().equals(cidade)) {
				return contatos;
			}
		}
		return null;
	}

	/*
	 * FIXME O metodo atualizar será refinado
	 */
	public static void atualizar(Contato contato) {
		Integer opcao;
		for (int i = 0; i < contatos.length; i++) {
			if(contatos[i].getNome().equals(contato.getNome())) { 
				System.out.println("O que deseja alterar neste contato? " + "Nome do contato " + contato.getNome());
				System.out.println("1 - Nome");
				System.out.println("2 - Cpf");
				System.out.println("3 - Email");
				System.out.println("4 - telefone");
				System.out.println("5 - logradouro");
				System.out.println("6 - Bairro");
				System.out.println("7 - Cidade");
				System.out.println("8 - Numero");
				System.out.println("9 - Data de nascimento");
				System.out.println("Por favor digite a sua opção: ");
				opcao = sc.nextInt();
				switch (opcao) {
				case 1:
					System.out.println("Digite o novo nome: ");
					String nome = sc.nextLine() + sc.nextLine();
					contatos[i].setNome(nome);
					break;
				case 2:
					System.out.println("Digite o novo cpf: ");
					String cpf = sc.nextLine();
					contatos[i].setCpf(cpf);
					break;
				case 3:
					System.out.println("Digite o novo Email: ");
					String email = sc.nextLine();
					contatos[i].setEmail(email);
					break;
				case 4:
					System.out.println("Digite o novo telefone: ");
					String telefone = sc.nextLine();
					contatos[i].setTelefone(telefone);
					break;
				case 5:
					System.out.println("Digite o novo logradouro: ");
					String logra = sc.nextLine();
					contatos[i].setLogradouro(logra);
					break;
				case 6:
					System.out.println("Digite o novo bairro: ");
					String bairro = sc.nextLine();
					contatos[i].setBairro(bairro);
					break;
				case 7:
					System.out.println("Digite a nova cidade: ");
					String cidade = sc.nextLine();
					contatos[i].setCidade(cidade);
					break;
				case 8:
					System.out.println("Digite o novo numero do bairro: ");
					Integer num = sc.nextInt();
					contatos[i].setNumero(num);
					break;
//				case 9:
//					System.out.println("Digite a nova data de nascimento: ");
//					String nome = sc.nextLine();
//					contatos[i].setNome(nome);
//					break;
				default:
					System.out.println("Opção invalida");
					break;
				}
			}
		}
	}

	
	public static void imprimirContato(Contato contato) {
		for (Contato contatoimpressao : contatos) {
			if(contatoimpressao.equals(contato)) { 
				System.out.println("Nome: " + contatoimpressao.getNome());
				System.out.println("Cpf: " + contatoimpressao.getCpf());
				System.out.println("Email: " + contatoimpressao.getEmail());
				System.out.println("telefone: " + contatoimpressao.getTelefone());
				System.out.println("logradouro: " + contatoimpressao.getLogradouro());
				System.out.println("Bairro: " + contatoimpressao.getBairro());
				System.out.println("Cidade: " + contatoimpressao.getCidade());
				System.out.println("Numero: " + contatoimpressao.getNumero());
				System.out.println("Data de nascimento: " + contatoimpressao.getDataNascimento());
				System.out.println();
				break;
			}
		}
	}	
	

	
	public static void imprimirTodosContatos() throws NullPointerException {
		for (Contato contato : contatos) {
			System.out.println("Nome: " + contato.getNome());
			System.out.println("Cpf: " + contato.getCpf());
			System.out.println("Email: " + contato.getEmail());
			System.out.println("telefone: " + contato.getTelefone());
			System.out.println("logradouro: " + contato.getLogradouro());
			System.out.println("Bairro: " + contato.getBairro());
			System.out.println("Cidade: " + contato.getCidade());
			System.out.println("Numero: " + contato.getNumero());
			System.out.println("Data de nascimento: " + contato.getDataNascimento());
			System.out.println();
			if(contato.equals(null)) { 
				throw new NullPointerException("Lista de contatos incompleta");
			}
		}
	}

	
	public static void ordenaPorCpf(boolean reverse) {
		// TODO Auto-generated method stub
		
	}

	
	public static void ordenaPorNome(boolean reverse) {
		for (int i = 0; i < contatos.length; i++) {
			if (contatos[i].getNome().startsWith("A") || contatos[i].getNome().startsWith("a")) {
				System.out.println(contatos[i]);
			}
		}
		
	}

	
	public static void ordenaPorDataDeNascimento(boolean reverse) {
		// TODO Auto-generated method stub
		
	}
	
}
