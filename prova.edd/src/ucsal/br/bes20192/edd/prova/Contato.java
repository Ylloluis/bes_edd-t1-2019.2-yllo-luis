package ucsal.br.bes20192.edd.prova;

public class Contato {
	private String cpf;
	private String nome;
	private String email;
	private Integer numero;
	private String telefone;
	private String logradouro;
	private String bairro;
	private String cidade;
	private Date dataNascimento;

	/**
	 * Construtor do objeto
	 */

	public Contato(String cpf, String nome, String email, Integer numero, String telefone, String logradouro,
			String bairro, String cidade, Date dataNascimento) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
		this.numero = numero;
		this.telefone = telefone;
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
		this.dataNascimento = dataNascimento;
	}

	/**
	 * Area de getters e setters
	 */

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getDataNascimento() {
		String data = dataNascimento.getDia() + "/" + dataNascimento.getMes() + "/" + dataNascimento.getAno();
		return data;
	}

	public void setDataNascimento(Date datanascimento) {
		this.dataNascimento = datanascimento;
	}

	
	
}