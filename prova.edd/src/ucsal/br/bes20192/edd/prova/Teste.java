package ucsal.br.bes20192.edd.prova;

import java.util.Scanner;

public class Teste {
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		executarMenu();		
	}
	
	public static void executarMenu() {
		Integer opcao = 0;
		while(opcao != 12) { 
			System.out.println("--- Menu de Operações ---");
			System.out.println("- 1 Inserir contato");
			System.out.println("- 2 Remover contato");
			System.out.println("- 3 Consultar por cpf");
			System.out.println("- 4 Consultar por bairro");
			System.out.println("- 5 Consultar por cidade");
			System.out.println("- 6 Atualizar Contato");
			System.out.println("- 7 Imprimir Contato");
			System.out.println("- 8 Imprimir todos os Contatos");
			System.out.println("- 9 Ordenar por Cpf");
			System.out.println("- 10 Ordernar por nome");
			System.out.println("- 11 Ordenar por data de nascimento");
			System.out.println("- 12 Fechar programa");
			System.out.println("Selecione a opção: ");
			opcao = sc.nextInt();
			switch (opcao) {
				case 1:
					Utilitaria.obterContato();
					break;	
				case 2:
					Utilitaria.removerContato();
					break;
				case 3:
					Utilitaria.consultarPorCpf();
					break;	
				case 4:
					Utilitaria.consultarPorBairro();
					break;
				case 5:
					Utilitaria.consultarPorCidade();
					break;
				case 6:
					Utilitaria.atualizarContato();
					break;
				case 7:
					Utilitaria.imprimirContato();
					break;
				case 8:
					Utilitaria.imprimirTodosContatos();
					break;
				case 9:
					Utilitaria.ordenarCpf();
					break;
				case 10:
					Utilitaria.ordenarNome();
					break;
				case 11: 
					Utilitaria.ordenarData();
					break;
				case 12: 
					System.exit(opcao);
					break;
			default:
				System.out.println("Operação invalida");
				break;
			}
		}
	}
	
}
