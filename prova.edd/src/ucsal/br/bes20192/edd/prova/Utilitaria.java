package ucsal.br.bes20192.edd.prova;

import java.util.Scanner;

public class Utilitaria {
	private static Scanner sc = new Scanner(System.in);
	/**
	 * Classe de utilitarios pensei em por as operações do programa aqui para obter uma classe main bem limpa...
	 * As operações que mexem com o vetor estão na classe agenda...
	 */
	
	public static void obterContato() {
		String nome, email, telefone, logradouro, bairro, cidade, cpf;
		Integer numero;
		String dia, mes, ano;
		
		System.out.println("Digite o nome do contato: ");
		nome = sc.nextLine();
		System.out.println("Digite o email do contato: ");
		email = sc.nextLine();
		System.out.println("Digite o telefone do contato: ");
		telefone = sc.nextLine();
		System.out.println("Digite o logradouro do contato: ");
		logradouro = sc.nextLine();
		System.out.println("Digite o bairro do contato: ");
		bairro = sc.nextLine();
		System.out.println("Digite o numero do bairro: ");
		numero = sc.nextInt();
		System.out.println("Digite o cidade do contato: ");
		cidade = sc.nextLine() + sc.nextLine();
		System.out.println("Digite o cpf do contato: ");
		cpf = sc.nextLine();
		System.out.println("Digite a data de nascimento dd/MM/AAAA");
		dia = sc.next();
		mes = sc.next();
		ano = sc.next();
		Date data = new Date(dia, mes, ano);
		Contato contato = new Contato(cpf, nome, email, numero, telefone, logradouro, bairro, cidade, data);
		Agenda.inserir(contato);
	}
	
	public static void removerContato() {
		String cpf;
		System.out.println("Digite qual contato deseja remover via cpf: ");
		cpf = sc.nextLine();
		cpf = sc.nextLine();
		try { 
			Agenda.remover(cpf);
		} catch(NullPointerException e) { 
			System.out.print("Lista de contatos incompleta/inexistente -> ");
			System.out.print(e instanceof NullPointerException);
			System.out.println();
		}
		
	}
	
	public static void consultarPorCpf() {
		// TODO Auto-generated method stub
		
	}
	
	public static void consultarPorBairro() {
		String bairro;
		System.out.println("Digite qual bairro deseja pesquisar: ");
		bairro = sc.nextLine();
		Agenda.consultarPorBairro(bairro);
	}
	
	public static void consultarPorCidade() {
		// TODO Auto-generated method stub
		
	}
	
	/*FIXME
	 * Problema no input do scanner
	 */
	public static void atualizarContato() {
		Contato[] contatosagenda = obtervetor();
		System.out.println("Qual contato deseja alterar? Por favor digite o nome do contato");
		String nome = sc.nextLine() + sc.nextLine();
		try { 
			Contato contatoobtido = procurarObjeto(nome, contatosagenda);
			if(contatoobtido == null) { 
				System.out.println("Contato não encontrado");
			} else { 
				Agenda.atualizar(contatoobtido);
			}
		} catch(NullPointerException e) { 
			System.out.print("Lista de contatos incompleta/inexistente -> ");
			System.out.print(e instanceof NullPointerException);
			System.out.println();
		}		
	}
	
	public static void ordenarData() {
		// TODO Auto-generated method stub
		
	}
	
	public static void ordenarCpf() {
		// TODO Auto-generated method stub	
	}
	
	public static void ordenarNome() {
		// TODO Auto-generated method stub
		
	}

	public static void imprimirTodosContatos() {
		try {
			Agenda.imprimirTodosContatos();
		} catch (NullPointerException e) {
			System.out.print("Lista de contatos incompleta/inexistente -> ");
			System.out.print(e instanceof NullPointerException);
			System.out.println();
		}
		
	}

	public static void imprimirContato() {
		Contato[] contatosagenda = obtervetor();
		System.out.println("Qual contato deseja imprimir? Por favor digite o nome do contato");
		String nome = sc.nextLine() + sc.nextLine();
		try { 
			Contato contatoobtido = procurarObjeto(nome, contatosagenda);
			if(contatoobtido == null) { 
				System.out.println("Contato não encontrado");
			} else { 
				Agenda.imprimirContato(contatoobtido);
			}
		} catch(NullPointerException e) { 
			System.out.print("Lista de contatos incompleta/inexistente -> ");
			System.out.print(e instanceof NullPointerException);
			System.out.println();
		}	
	}

	private static Contato procurarObjeto(String nome, Contato[] contatosagenda) throws NullPointerException {
		for (Contato contato : contatosagenda) {
			if(contato.getNome().equals(nome)) { 
				return contato;
			} else if(contato.equals(null)) { 
				throw new NullPointerException("Lista de contatos incompleta");	
			}	
		}		
		return null;
	}

	private static Contato[] obtervetor() {
		Contato[] contatosagenda = Agenda.contatos;
		return contatosagenda;
	}

	
}
