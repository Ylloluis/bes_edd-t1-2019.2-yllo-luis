package ucsal.br20192.t1.edd.ted;

public interface ILMD {

    void cadastrarproduto (int codigo, String descricao );
    void removerproduto (int codigo);
    Noc consultaProduto (int codigo);
    void alterarProduto (int codigo, String descricao);
    void imprimeTudo();
    void imprimeCategoria(int codigo);
    void insereNo(int codigo, int valor);
    void removerCategoria(int codigo, int valor);
    No buscarCategoria(int codigo,int valor);
    void alterarCategoria(int codigo, int valor, int novoValor);

}
