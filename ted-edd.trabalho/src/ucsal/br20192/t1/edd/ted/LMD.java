package ucsal.br20192.t1.edd.ted;

public class LMD implements ILMD {

	Noc inicio = null;

	public void cadastrarproduto(int codigo, String descricao) {

		Noc nocNovo = new Noc();
		nocNovo.codigo = codigo;
		nocNovo.descricao = descricao;
		nocNovo.ini = null;

		if (inicio == null) {
			inicio = nocNovo;
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null) {
				nocAux = nocAux.prox;
			}

			nocAux.prox = nocNovo;
		}

	}

	public void removerproduto(int codigo) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			Noc nocAnt = null;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAnt = nocAux;
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				if (nocAnt == null) {
					inicio = inicio.prox;
				} else if (nocAux.prox == null) {
					nocAnt.prox = null;
					nocAux = null;
				} else {
					nocAnt.prox = nocAux.prox;
					nocAux = null;
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}
	}

	public Noc consultaProduto(int codigo) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				return nocAux;
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

		return null;
	}

	public void alterarProduto(int codigo, String descricao) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				nocAux.descricao = descricao;
			} else {
				System.out.println("Categoria n�o encontrada!");
			}

		}

	}

	public void imprimeTudo() {

		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux != null) {
				System.out.print(nocAux.descricao + " ");
				if (nocAux.ini != null) {
					No noAux = nocAux.ini;
					while (noAux != null) {
						System.out.print(noAux.valor + " ");
						noAux = noAux.prox;
					}
				}
				nocAux = nocAux.prox;
				System.out.print(", ");
			}
		}
		System.out.print(" ");
	}

	public void imprimeCategoria(int codigo) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				System.out.print(nocAux.descricao + " ");
				if (nocAux.ini != null) {
					No noAux = nocAux.ini;
					while (noAux != null) {
						System.out.print(noAux.valor + " ");
						noAux = noAux.prox;
					}
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}

	public void insereNo(int codigo, int valor) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				No noNovo = new No();
				noNovo.valor = valor;

				if (nocAux.ini == null) {
					nocAux.ini = noNovo;
				} else {
					No noAux = nocAux.ini;
					while (noAux.prox != null) {
						noAux = noAux.prox;
					}

					noAux.prox = noNovo;
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}

	public void removerCategoria(int codigo, int valor) {

		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				if (nocAux.ini == null) {
					System.out.println("Elemento n�o encontrado!");
				} else {
					No noAux = nocAux.ini;
					No noAnt = null;

					while (noAux.prox != null && noAux.valor != valor) {
						noAnt = noAux;
						noAux = noAux.prox;
					}
					if (noAux.valor == valor) {
						if (noAnt == null) {
							nocAux.ini = nocAux.ini.prox;
						} else if (noAux.prox == null) {
							noAnt.prox = null;
							noAux = null;
						} else {
							noAnt.prox = noAux.prox;
							noAux = null;
						}
					} else {
						System.out.println("n�o encontrado!");
					}
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}

	public No buscarCategoria(int codigo, int valor) {
		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				if (nocAux.ini == null) {
					System.out.println("Elemento n�o encontrado!");
				} else {
					No noAux = nocAux.ini;
					while (noAux.prox != null && noAux.valor != valor) {
						noAux = noAux.prox;
					}
					if (noAux.valor == valor) {
						return noAux;
					} else {
						System.out.println("Elemento n�o encontrado!");
					}
				}

			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

		return null;
	}

	public void alterarCategoria(int codigo, int valor, int novoValor) {

		if (inicio == null) {
			System.out.println("Lista vazia!");
		} else {
			Noc nocAux = inicio;
			while (nocAux.prox != null && nocAux.codigo != codigo) {
				nocAux = nocAux.prox;
			}
			if (nocAux.codigo == codigo) {
				No noNovo = new No();
				noNovo.valor = valor;

				if (nocAux.ini == null) {
					System.out.println("n�o encontrado!");
				} else {
					No noAux = nocAux.ini;

					while (noAux.prox != null && noAux.valor != valor) {
						noAux = noAux.prox;
					}
					if (noAux.valor == valor) {
						noAux.valor = novoValor;
					} else {
						System.out.println("n�o n�o encontrado!");
					}
				}
			} else {
				System.out.println("Categoria n�o encontrada!");
			}
		}

	}
}
