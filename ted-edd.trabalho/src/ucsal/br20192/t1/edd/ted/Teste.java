package ucsal.br20192.t1.edd.ted;

public class Teste {
    public static void main(String[] args) {

        executa(new LMD());

    }

    public static void executa(ILMD lista){

        System.out.println("Teste insere------------------------------------------------------------------------");

        lista.cadastrarproduto(1, "Papelaria");
        lista.cadastrarproduto(2, "Material de limpeza");
        lista.cadastrarproduto(3, "Alimentos");
        lista.cadastrarproduto(4, "Eletroicos");
        lista.cadastrarproduto(5, "Moveis");

        System.out.println("Esperado: Papelaria, Material de limpeza, Alimentos, Eletroicos,Moveis");
        lista.imprimeTudo();
        System.out.println(" ");

        System.out.println("Teste alterar categoria------------------------------------------------------------------------");

        lista.alterarProduto(1, "Nova papelaria");
        lista.alterarProduto(3, "Novo alimentos");
        lista.alterarProduto(5, "Novo Moveis");

        System.out.println("Esperado: Nova papelaria, Material de limpeza, Novo alimentos, Eletroicos, Novo Moveis");
        lista.imprimeTudo();
        System.out.println(" ");

        System.out.println("Esperado: Categoria não encontrada!");
        lista.alterarProduto(7, "Novo eletrodomésticos");
        System.out.println(" ");

        System.out.println("Teste buscar------------------------------------------------------------------------");

        System.out.println("Esperado: Nova papelaria");
        Noc no1 = lista.consultaProduto(1);
        if (no1!=null)
            System.out.println(no1.descricao);

        System.out.println("\nEsperado: Novo alimentos");
        Noc no2 = lista.consultaProduto(3);
        if (no2!=null)
            System.out.println(no2.descricao);

        System.out.println("\nEsperado: Novo Moveis");
        Noc no3 = lista.consultaProduto(5);
        if (no3!=null)
            System.out.println(no3.descricao);
        System.out.println(" ");

        System.out.println("Teste insere nó------------------------------------------------------------------------");


        lista.insereNo(1, 1);
        lista.insereNo(1, 2);
        lista.insereNo(1, 3);

        lista.insereNo(3, 4);
        lista.insereNo(3, 5);

        lista.insereNo(5, 6);

        System.out.println("Esperado: Nova papelaria 1 2 3, Material de limpeza, Novo alimentos 4 5, Eletrônicos, Novo móveis 6");
        lista.imprimeTudo();
        System.out.println("\n ");

        System.out.println("Teste alterar nó------------------------------------------------------------------------");

        lista.alterarCategoria(1, 1, 10);
        lista.alterarCategoria(3, 5, 50);
        lista.alterarCategoria(5, 6, 60);

        System.out.println("Esperado: Nova papelaria 10 2 3, Material de limpeza, Novo alimentos 4 50, Eletrônicos, Novo móveis 60");
        lista.imprimeTudo();
        System.out.println("\n ");

        System.out.println("Esperado: Nao nao encontrado!");
        lista.alterarCategoria(5, 7, 70);
        System.out.println("\n");

        System.out.println("Teste buscar no------------------------------------------------------------------------");

        System.out.println("Esperado: 3");
        No no4 = lista.buscarCategoria(1, 3);
        if(no4!=null) {
            System.out.println(no4.valor);
        }

        System.out.println("Esperado: 50");
        No no5 = lista.buscarCategoria(3, 50);
        if(no5!=null) {
            System.out.println(no5.valor);
        }


        System.out.println("Esperado: 60");
        No no6 = lista.buscarCategoria(5, 60);
        if(no6!=null) {
            System.out.println(no6.valor);
        }


        System.out.println("Esperado: Elemento nao encontrado!");
        No no7 = lista.buscarCategoria(5, 70);
        if(no7!=null) {
            System.out.println(no7.valor);
        }
        System.out.println(" ");

        System.out.println("Teste remover no------------------------------------------------------------------------");

        lista.removerCategoria(1, 2);
        System.out.println("Esperado: Nova papelaria 10 3, Material de limpeza, Novo alimentos 4 50, Eletronicos, Novo moveis 60");
        lista.imprimeTudo();
        System.out.println("\n ");

        lista.removerCategoria(3, 50);
        System.out.println("Esperado: Nova papelaria 10 3, Material de limpeza, Novo alimentos 4, Eletronicos, Novo moveis 60");
        lista.imprimeTudo();
        System.out.println("\n ");

        lista.removerCategoria(5, 60);
        System.out.println("Esperado: Nova papelaria 10 3, Material de limpeza, Novo alimentos 4,Eletronicos, Novo moveis");
        lista.imprimeTudo();

        System.out.println("\n ");
        System.out.println("Esperado: Elemento não encontrado!");
        lista.removerCategoria(5, 70);
        System.out.println(" ");

        System.out.println("Teste imprimir categoria------------------------------------------------------------------------");

        System.out.println("Esperado: Nova papelaria 10 3");
        lista.imprimeCategoria(1);
        System.out.println("\n ");

        System.out.println("Esperado: Novo alimentos 4");
        lista.imprimeCategoria(3);
        System.out.println("\n ");

        System.out.println("Esperado: Novo moveis");
        lista.imprimeCategoria(5);
        System.out.println("\n ");

        System.out.println("Esperado: Categoria nao encontrada!");
        lista.imprimeCategoria(7);
        System.out.println("\n");

        System.out.println("Teste remover categoria------------------------------------------------------------------------");

        lista.removerproduto(3);
        System.out.println("Esperado: Nova papelaria 10 3, Material de limpeza, Eletronicos, Novo moveis");
        lista.imprimeTudo();
        System.out.println("\n");

        lista.removerproduto(1);
        System.out.println("Esperado: Material de limpeza, Eletronicos, Novo moveis");
        lista.imprimeTudo();
        System.out.println("\n ");

        lista.removerproduto(5);
        System.out.println("Esperado: Material de limpeza, Eletronicos");
        lista.imprimeTudo();
    }
}
