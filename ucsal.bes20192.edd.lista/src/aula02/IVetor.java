package aula02;


public interface IVetor {

	int produtoPorEscalar();
	void somaDosElementos(int valor);
	void numeroDeElementos();
	void leitura();
	void imprime();
	
}
