package aula02;


public class Teste {

	public static void main(String[] args) {
		executar(new Vetor());
	}

	public static void executar(IVetor vetor) {
		int valor = 0;
		vetor.leitura();
		valor = vetor.produtoPorEscalar();
		vetor.somaDosElementos(valor);
	}

}
