package aula3;

public interface IString {
	int length();
	char charAt(int posicao);
	boolean equals(String valor);
	boolean startsWith(String valor);
	boolean endsWith(String valor);
	int indexOf(char letra);
	int lastIndexOf(char letra);
	String substring(int inicio, int qtDeCaracteres);
	String replace(char letraASertrocada, char letraATrocar);
	String concat(String valor);
	void imprime();
}
