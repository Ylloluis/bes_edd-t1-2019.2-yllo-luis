package aula3;

import java.util.*;

public class String implements IString {	
	char[] vetor;
	
	public String(int valor) {
		vetor = new char[valor];
	}
	
	// Funcional - Pegar a palavra manda para o vetor
	public String() {
		@SuppressWarnings("resource")
		Scanner entrada = new Scanner(System.in);
		System.out.print("Informe a palavra do tamanho escolhido: ");
		vetor = entrada.nextLine().trim().toCharArray();
	}

	// Funcional - Diz tamanho da palavra (vetor)
	public int length() {
		System.out.println("Tamanho da palavra: " + vetor.length);
		return vetor.length;
	}

	// Funcional - Informa a letra na posi��o determinada
	public char charAt(int posicao) {
		System.out.println(vetor[posicao]);
		return vetor[posicao];
	}

	//Funcional - Compara valores entre objetos 
	public boolean equals(String valor) {
		boolean resultado = false;	
		if(valor.vetor.length > vetor.length) { 
				resultado = false;
			} else { 
				for (int i = 0; i < vetor.length; i++) {
					if(valor.vetor[i] == vetor[i]) { 
						resultado = true;
					} else { 
						resultado = false;
					}
				}
			}
			
		return resultado;
	}

	
	public boolean startsWith(String valor) {
		boolean check = false;
		for (int i = 0; i < vetor.length; i++) {
			if(valor.vetor[i] != 32) { 
				check = vetor[i] == valor.vetor[i];
			} else { 
				break;
			}
			
		}
		return check;	
	}

	
	public boolean endsWith(String valor) {
		boolean check = false;
		for (int i = valor.vetor.length - 1; i > 0; i--) {
			if(valor.vetor[i] != 32) { 
				check = vetor[i] == valor.vetor[i];
			} else { 																			
				break;
			}
			
		}
		return check;	
	}

	// Funcional - Informa onde a letra selecionada se encontra
	public int indexOf(char letra) {
		int i;
		for (i = 0; i < vetor.length; i++) {
			if(vetor[i] == letra) { 
				System.out.print(i + " ");
			}
		}	
		return i;
	}

	// Funcional - informa a ultima letra 
	public int lastIndexOf(char letra) {
		int aux = -1;
		for (int i = 0; i < vetor.length; i++) {
			if(vetor[i] == letra) { 
				aux = i;
			}
		}
		System.out.println(aux);
		return aux;
	}

	
	public String substring(int inicio, int qtDeCaracteres) {
		String sub = new String(qtDeCaracteres);
		for (int i = inicio; i < qtDeCaracteres; i++) {
			sub.vetor[i] = vetor[i];
		}
		return sub;
	}

	// Funcional - Troca uma palavra pela outra na posição da mesma
	public String replace(char letraASertrocada, char letraATrocar) {
		for (int i = 0; i < vetor.length; i++) {
			if(vetor[i] == letraASertrocada) { 
				vetor[i] = letraATrocar;			
			}
		}
		return this;
	}

	
	public String concat(String valor) {
		String sb = new String(12);
		for (int i = 0, aux = 0; i < sb.vetor.length; i++) {
			if(i < 6) { 
				sb.vetor[i] += vetor[i];
			} else { 
				sb.vetor[i] += vetor[aux];
				aux++;
			}
			
		}
		
		return sb;
	}

	
	public void imprime() {
		System.out.print("Palavra: ");
		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i]);
		}
		System.out.println();
		
	}
	
}
