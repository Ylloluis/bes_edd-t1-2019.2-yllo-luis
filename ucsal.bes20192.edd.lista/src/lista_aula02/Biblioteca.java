package lista_aula02;

import java.util.Scanner;

public class Biblioteca {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		obterVetor();
	}
	
	public static Livro[] obterVetor() {
		Livro[] vetor_livro = new Livro[4];
		vetor_livro = LivroConcreto.criarVetor();
		buscaLivro(vetor_livro);
		return vetor_livro;
	}


	public static void buscaLivro(Livro[] vetor_livro) {
		System.out.println("Por favor digite um autor para a busca: ");
		String livro = sc.nextLine();
		System.out.println("Por favor digite um genero para a busca: ");
		String genero = sc.nextLine();
		for (int i = 0; i < vetor_livro.length; i++) {
			if(livro.equals(vetor_livro[i].getAutor()) && genero.equals(vetor_livro[i].getGenero())) { 
				System.out.println("Titulo: " + vetor_livro[0].getTitulo());
				System.out.println("Autor: " + vetor_livro[0].getAutor());
				System.out.println("Genero: " + vetor_livro[0].getGenero());
				System.out.println("Ano: " + vetor_livro[0].getAno());
				break;
			} else { 
				System.out.println("Null");
			}
		}
	}
	
	
}
