package lista_aula02;

public interface Ilivro {
	public static Livro livroCria(String titulo, String autor, String genero, int ano) {
		Livro livro = new Livro(titulo, autor, genero, ano);
		return livro;
	}
	String getGenero();	
	String getAutor();
	String getTitulo(); 
	int getAno();
	int livroVerificaNoModernismo();
}
