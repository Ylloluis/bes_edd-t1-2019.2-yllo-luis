package lista_aula02;

public class Livro {
	 String titulo;
	 String autor;
	 String genero;
	 int ano;

	public Livro(String titulo, String autor, String genero, int ano) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.ano = ano;
	}
	
	public  String getTitulo() {
		return titulo;
	}
	
	public  String getAutor() {
		return autor;
	}
	public  String getGenero() {
		return genero;
	}
	public  int getAno() {
		return ano;
	}
 
}
