package lista_aula02;

    public class LivroConcreto {
    	String titulo;
    	String autor;
    	String genero;
    	static int ano;
    	    	
    	public static Livro[] criarVetor() { 
    		Livro[] Livro = new Livro[4];
    		Livro[0] = LivroConcreto.livroCria("Novos Poemas", "Vinicius de Moraes", "poesia", 1938);
    		Livro[1] = LivroConcreto.livroCria("Poemas Escritos na Índia", "Cecília Meireles", "poesia", 1962);
    		Livro[2] = LivroConcreto.livroCria("Orfeu da Conceição", "Vinicius de Moraes", "teatro", 1954);
    		Livro[3] = LivroConcreto.livroCria("Ariana, a Mulher", "Vinicius de Moraes", "poesia", 1936);
    		
    		return Livro;
    	}
    	
    	public String getTitulo() {
    		return titulo;
    	}


    	public String getAutor() {
    		return autor;
    	}


    	public String getGenero() {
    		return genero;
    	}


    	public int getAno() {
    		return ano;
    	}
    	
    	public static Livro livroCria(String titulo, String autor, String genero, int ano) {
    		Livro livro = new Livro(titulo, autor, genero, ano);
    		livro.titulo = titulo;
    		livro.genero = genero;
    		livro.autor = autor;
    		livro.ano = ano;
    		return livro;
    	}
    	
    	public static int livroVerificaNoModernismo() {
    		if(ano > 1930 && ano < 1945) { 
    			return 1;
    		} else { 
    			return 0;
    		}	
    	}
    	

    }
	
	


