/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

import javax.swing.*;
/**
 *
 * @author ylloluis
 */
public class Questao1 {
// Desenvolva um programa para ler uma temperatura em graus Celsius e imprimir o
// valor converƟdo para Fahrenheit. A fórmula de conversão é °F = °C × 1,8 + 32
    public static void main(String[] args) {
        obtertemp();
    }

    public static int obtertemp() {
        int temp1 = 0;
        try {
            temp1 = Integer.parseInt(JOptionPane.showInputDialog(null,"Por favor digite uma temperatura em Celsius", "Calculador de temperatura" ,JOptionPane.QUESTION_MESSAGE));
            if((temp1 == 800) || (temp1 == -500)) { 
                JOptionPane.showMessageDialog(null,"Temperatura invalida", "Calculador de temperatura" ,JOptionPane.ERROR_MESSAGE);
            } else {
                calctemp(temp1);
            }
        } catch(Exception e) { 
            JOptionPane.showMessageDialog(null,"Temperatura invalida", "Calculador de temperatura" ,JOptionPane.ERROR_MESSAGE);
            obtertemp();
        }   
       return temp1; 
    }

    public static void calctemp(int temp1) {
        double res = temp1 * 1.8 + 32;
        JOptionPane.showMessageDialog(null, "Resultado: " + res, "Calculo de temperatura", JOptionPane.PLAIN_MESSAGE);
        int conf = JOptionPane.showConfirmDialog(null, "Deseja calcular outra temperatura?", "Calculador de temperatura", JOptionPane.INFORMATION_MESSAGE);
        if(conf == JOptionPane.YES_OPTION) { 
            obtertemp();
        } else {
            System.exit(conf);
        }
    }
}
