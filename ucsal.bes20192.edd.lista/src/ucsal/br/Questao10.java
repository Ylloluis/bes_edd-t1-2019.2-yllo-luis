/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;
import java.util.*;
/**
 *
 * @author ylloluis
 */
public class Questao10 {
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        startvetor();
    }

    public static int[] startvetor() {
        int[] vetor = new int[10];
        for (int i = 0; i < vetor.length; i++) {
            System.out.println("Digite um número: ");
            int num = sc.nextInt();
            vetor[i] = num;
        }
        contvetor(vetor);
        return vetor;
    }

    public static void contvetor(int[] vetor) {
        Arrays.sort(vetor);
        System.out.println("Maior valor: " + vetor[9]);
        System.out.println("Menor valor: " + vetor[0]);
    }
}
