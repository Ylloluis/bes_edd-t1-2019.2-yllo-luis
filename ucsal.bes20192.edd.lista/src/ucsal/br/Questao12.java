package ucsal.br;

import java.math.*;
import java.util.Scanner;;
/**
 *
 * @author ylloluis
 */
public class Questao12 {
    static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
        double base = obterNumero();
        obterEx(base);
    }
	
	public static double obterNumero() {
		System.out.print("Digite um número: ");
		Double num = sc.nextDouble();
		System.out.println();
		return num;
	}
	
	
	public static void obterEx(double base) {
		System.out.print("Digite um número: ");
		double a = sc.nextDouble();
		System.out.println();
		calc(base, a);
	}
	
	public static void calc(double base, double a) {
		Double resultado = Math.pow(base,a);
		System.out.println("Resultado: " + resultado);
	}

	
}
