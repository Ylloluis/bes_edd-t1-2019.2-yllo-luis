/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;
import java.util.*;
/**
 *
 * @author ylloluis
 */
public class Questao13 {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        startVetor();
    }

    public static int[] startVetor() {
        int[] vetor = new int[10];
        Random gerador = new Random();
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = gerador.nextInt(500); 
        }
        Calcvet(vetor);
        return vetor;
    }

    public static void Calcvet(int[] vetor) {
        boolean found = false;
        int pos = 0;
        System.out.print("Por favor digite um número: ");
        int num = sc.nextInt();
        for (int i = 0; i < 10; i++) {
            if(num == vetor[i]) { 
                found = true;
                pos = i;
            }
        }
        if(found == true) { 
            System.out.println("Número encontrado\nposição: " + pos);
        } else {
            System.out.println("Número não encontrado");
        }
    }
}
