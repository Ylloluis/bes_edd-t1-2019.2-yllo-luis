/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

import java.util.*;
/**
    Escreva um programa que gere e imprima uma matriz M 4x4 com valores
    aleatórios entre 0-9. Após isso determine o maior número da matriz e a sua posição
    (linha, coluna).
 */
public class Questao14 {
    public static void main(String[] args) {
        startvetor();
    }

    public static int[][] startvetor() {
        int[][] vet_bi = new int[4][4];
        Random ran = new Random();
        for (int i = 0; i < vet_bi.length; i++) {
            for (int j = 0; j < vet_bi.length; j++) {
                  vet_bi[i][j] = ran.nextInt(10);  
            }    
        }
        calcvet(vet_bi);
        return vet_bi;
    }

    public static void calcvet(int[][] vet_bi) {
    	int maior_num = 0, linha = 0, coluna = 0; 
    	for (int i = 0; i < vet_bi.length; i++) {
			for (int j = 0; j < vet_bi[i].length; j++) {
				if(vet_bi[i][j] > maior_num) {
					maior_num = vet_bi[i][j];
				    coluna = i;
					linha = j;
				}
			}
    	}
    	System.out.println("Maior número: " + maior_num + "\n" + "linha: " + linha + "\n" + "Coluna: " + coluna);
    }
}