package ucsal.br;

import java.util.Random;

//Escreva um programa que gere e imprima uma matriz M 10x10 com valores
//aleatórios entre 0-9. Após isso indique qual é o maior e o menor valor da linha 5 e qual
//é o maior e o menor valor da coluna 7.

public class Questao15 {
	
	public static void main(String[] args) {
        startvetor();
    }

    public static int[][] startvetor() {
        int[][] vet_bi = new int[10][10];
        Random ran = new Random();
        for (int i = 0; i < vet_bi.length; i++) {
            for (int j = 0; j < vet_bi.length; j++) {
                  vet_bi[i][j] = ran.nextInt(10);  
            }    
        }
        calcvet(vet_bi);
        return vet_bi;
    }

    public static void calcvet(int[][] vet_bi) {
    	int maior_num_linha5 = 0, maior_num_linha7 = 0; 
    	for (int i = 0; i < vet_bi.length; i++) {
			for (int j = 0; j < vet_bi[i].length; j++) {
				if(vet_bi[i][5] > maior_num_linha5) {
					maior_num_linha5 = vet_bi[i][j];
				}
				if(vet_bi[7][j] > maior_num_linha7) {
					maior_num_linha7 = vet_bi[i][j];
				}
			}
    	}
    	System.out.println("Maior número da linha 5: " + maior_num_linha5 + "\n" + "Maior número da coluna 7: " + maior_num_linha7);
    }
}


