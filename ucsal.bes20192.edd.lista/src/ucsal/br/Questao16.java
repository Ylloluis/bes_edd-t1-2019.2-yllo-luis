package ucsal.br;

import java.util.Random;

public class Questao16 {

	public static void main(String[] args) {
		startvet();
	}

	public static int[][] startvet() {
		Random ran = new Random();
		int[][] vet_bi = new int[3][3];
		for (int i = 0; i < vet_bi.length; i++) {
			for (int j = 0; j < vet_bi[i].length; j++) {
				vet_bi[i][j] = ran.nextInt(20);
			}
		}
		calcvet(vet_bi);
		return vet_bi;
	}

	public static void calcvet(int[][] vet_bi) {
		int par = 0;
		int impar = 0;
		for (int i = 0; i < vet_bi.length; i++) {
			for (int j = 0; j < vet_bi[i].length; j++) {
				if(vet_bi[i][j] % 2 == 0) { 
					par++;
				} else { 
					impar++;
				}
			}
		}
		System.out.println("Número de números pares: " + par);
		System.out.println("Número de números impares: " + impar);
		
		imprimirvet(vet_bi);
	}

	public static void imprimirvet(int[][] vet_bi) {
		for (int i = 0; i < vet_bi.length; i++) {
			for (int j = 0; j < vet_bi[i].length; j++) {
				System.out.print("|" + vet_bi[i][j] + "|");			
			}
			System.out.println();
		}
	}
	
}
