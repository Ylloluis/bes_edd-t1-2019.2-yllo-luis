package ucsal.br;

import java.util.Random;

//Escreva um programa que gere e imprima duas matrizes A e B 5x5 com valores
//aleatórios entre 0-4. Após isso calcule a matriz C, resultante da soma da matriz A com a
//matriz B. Imprimir a matriz C.

public class Questao17 {
	static Random ran = new Random();
	public static void main(String[] args) {
		gerarvetor_A();
	}

	public static int[][] gerarvetor_A() {
		int[][] vet_biA = new int[5][5];
		for (int i = 0; i < vet_biA.length; i++) {
			for (int j = 0; j < vet_biA[i].length; j++) {
				vet_biA[i][j] = ran.nextInt(4);
			}
		}
		gerarvetor_B(vet_biA);
		return vet_biA;
	}

	public static int[][] gerarvetor_B(int[][] vet_biA) {
		int[][] vet_biB = new int[5][5];
		for (int i = 0; i < vet_biB.length; i++) {
			for (int j = 0; j < vet_biB[i].length; j++) {
				vet_biB[i][j] = ran.nextInt(4);
			}
		}
		calcvetor(vet_biA,vet_biB);
		return vet_biB;	
	}

	public static void calcvetor(int[][] vet_biA, int[][] vet_biB) {
		int[][] vet_biC = new int[5][5];
		for (int i = 0; i < vet_biC.length; i++) {
			for (int j = 0; j < vet_biC[i].length; j++) {
				vet_biC[i][j] = vet_biA[i][j] + vet_biB[i][j];
			}
		}
		imprimirvetor(vet_biC);
	}

	public static void imprimirvetor(int[][] vet_biC) {
		for (int i = 0; i < vet_biC.length; i++) {
			for (int j = 0; j < vet_biC[i].length; j++) {
				System.out.print(vet_biC[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
}
