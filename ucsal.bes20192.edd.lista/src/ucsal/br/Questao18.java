package ucsal.br;
	//Escreva um programa para ler um array A de 5 elementos, e um array B de 8
	//elementos. Crie um array C intercalando A e B. Imprima todos os arrays.
public class Questao18 {

	public static void main(String[] args) {
		gerarvetor_A();
	}

	public static int[] gerarvetor_A() {
		int[] vet_A = new int[5];
		int acum = 2;
		for (int i = 0; i < vet_A.length; i++) {
			vet_A[i] = acum;
			acum += 2;
		}
		gerarvetor_B(vet_A);
		return vet_A;
	}

	public static int[] gerarvetor_B(int[] vet_A) {
		int[] vet_B = new int[8];
		int acum = 1;
		for (int i = 0; i < vet_B.length; i++) {
			vet_B[i] = acum;
			acum += 3;
		}
		gerarvetor_C(vet_A,vet_B);
		return vet_B;
	}

	public static void gerarvetor_C(int[] vet_A, int[] vet_B) {
		int[] vet_C = new int[13];
	    int num = 0;
	    int cont_B = 0;
	    int cont_A = 0;
	    while(num < vet_C.length) {
	    	if(num < 8 && cont_B < 8) {
	    		vet_C[num] = vet_B[cont_B];
	    		cont_B++;
	    		num++;
	    	} else {
	    		vet_C[num] = vet_A[cont_A];
	    		num++;
	    		cont_A++;
	    	}
	    }
	   for (int i = 0; i < vet_C.length; i++) {
			System.out.println(vet_C[i] + " ");
		}
	}

}
