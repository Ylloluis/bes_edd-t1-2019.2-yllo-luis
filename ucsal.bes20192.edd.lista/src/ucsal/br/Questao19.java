package ucsal.br;

import java.util.*;

	//Escreva um programa para ler um array de números int de 20 elementos, calcular
	//o fatorial de cada elemento, verificar se cada elemento do vetor é primo ou não,
	//verificar o maior e o menor dentre eles.


public class Questao19 {
	static Scanner sc = new Scanner(System.in);
	static final int NUM_FATORIAL = 0;
	static final int NUM_VETOR = 5;
	
	public static void main(String[] args) {
		long[] vetor = startvetor();
		long[] vetor2 = startvetor_2();
		obtervetor(vetor, vetor2);

	}

	public static long[] startvetor() {
		long[] vetor = new long[NUM_VETOR];
		return vetor;
	}
	
	public static long[] startvetor_2() {
		long[] vetor2 = new long[NUM_VETOR];
		return vetor2;
	}
	
	public static void obtervetor(long[] vetor, long[] vetor2) {
		long num = 0;
		for (int i = 0; i < vetor.length; i++) {
			System.out.print("Digite um número: ");
			num = sc.nextLong();
			vetor[i] = num;
			vetor2[i] = vetor[i];
			vetor[i] = calcularFatorial(vetor, i);
		}
		printarNumero(vetor, vetor2);
	}
	
	

	public static void printarNumero(long[] vetor, long[] vetor2) {
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Resultado fatorial: " + vetor[i] + " " + "\n" + "Elemento: " + vetor2[i] + " primo(True/False): "+ calcularNumPrimo(vetor2, i));
		}
		
	}
	
	/*
	 * Metodo ainda não funcional... 
	 */
	public static boolean calcularNumPrimo(long[] vetor, int index) {
		boolean primo;
		primo = vetor[index] % 2 == 0 && vetor[index] % 3 == 0 && vetor[index] % 5 == 0 && vetor[index] % 7 == 0 && vetor[index] % 11 == 0;	
		return primo;
	}

	public static long calcularFatorial(long[] vetor, int i) {
		long num = vetor[i];
		
		for (int j = (int) (num - 1); j > NUM_FATORIAL; j--) {
			num *= j;
		}
		return num;
	}

}
