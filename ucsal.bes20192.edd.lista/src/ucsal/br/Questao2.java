/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

import javax.swing.*;
/**
 *
 * @author ylloluis
 */
public class Questao2 {
//    Desenvolva um programa para ler o nome de uma pessoa, o seu peso e a sua
//    altura. Calcule o IMC dessa pessoa ( IMC = peso/altura² ). Imprima uma mensagem
//    informando qual o IMC e em que faixa de classificação a pessoa se encontra baseada
//    na tabela abaixo.
    public static void main(String[] args) {
        obterAltura();
    }

    public static double obterAltura() {
        double altura = 0.0;
        try { 
            altura = Double.parseDouble(JOptionPane.showInputDialog(null, "Por favor digite sua altura \nLembre-se de digitar a altura da seguinte forma \nExemplo: 1.60", "IMC", JOptionPane.QUESTION_MESSAGE));
            if((altura == 0) && (altura == 900)) {
                JOptionPane.showMessageDialog(null, "Altura invalida", "IMC", JOptionPane.ERROR_MESSAGE);
                JOptionPane.showMessageDialog(null, "Lembre-se de digitar a altura da seguinte forma \n Exemplo: 1.60", "IMC", JOptionPane.ERROR_MESSAGE);
                obterAltura();
            } else { 
                obterPeso(altura);
            }
        } catch(Exception e) { 
            JOptionPane.showMessageDialog(null, "Altura invalida", "IMC", JOptionPane.ERROR_MESSAGE);
            obterAltura();
        }
        return altura;
    }

    public static double obterPeso(double altura) {
        double peso = 0.0;
        try { 
            peso = Double.parseDouble(JOptionPane.showInputDialog(null, "Por favor digite sua peso", "IMC", JOptionPane.QUESTION_MESSAGE));
            if((peso == 0) || (peso >= 340)) {
                JOptionPane.showMessageDialog(null, "Peso invalida", "IMC", JOptionPane.ERROR_MESSAGE);                
                obterPeso(altura);
            } else { 
                calcIMC(altura, peso);
            }
        } catch(Exception e) { 
            JOptionPane.showMessageDialog(null, "Peso invalida", "IMC", JOptionPane.ERROR_MESSAGE);
            obterPeso(altura);
        }
        calcIMC(altura,peso);
        return peso;
    }

    public static void calcIMC(double altura, double peso) {
        double IMC = peso / (altura * altura);
        if(IMC <= 18.5) { 
            JOptionPane.showMessageDialog(null, "O paciente se encontra abaixo do peso!", "IMC", JOptionPane.WARNING_MESSAGE);
        } else if((IMC >= 18.5) && (IMC <= 25.0)) { 
            JOptionPane.showMessageDialog(null, "O paciente se encontra em peso normal!", "IMC", JOptionPane.PLAIN_MESSAGE);
        } else if((IMC > 25.0) && (IMC <= 30.0)) { 
            JOptionPane.showMessageDialog(null, "O paciente se encontra acima do peso!", "IMC", JOptionPane.WARNING_MESSAGE);
        } else if(IMC > 30.0) { 
            JOptionPane.showMessageDialog(null, "O paciente se encontra obeso!", "IMC", JOptionPane.WARNING_MESSAGE);
        }
        int conf = JOptionPane.showConfirmDialog(null, "Deseja calcular outro IMC?", "IMC", JOptionPane.INFORMATION_MESSAGE);
        
        if(conf == JOptionPane.YES_OPTION) {
            obterAltura();
        } else {
            System.exit(conf);
        }
    }
}
