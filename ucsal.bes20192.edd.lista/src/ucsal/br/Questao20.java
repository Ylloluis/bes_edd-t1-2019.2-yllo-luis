package ucsal.br;

import java.util.*;

//Crie um programa que mantenha em um array de 10 posições as 10 maiores
//pontuações do ENEM por ordem decrescente. Ao inserir uma nova nota o programa
//deverá iden ficar em qual posição ela deverá ser registrada e reorganizar o array.


public class Questao20 {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		int vetor[] = Obtervetor();
		Obternota(vetor);
	}

	public static int[] Obtervetor() {
		int[] vetor = new int[10];
 		return vetor;
	}
	
	public static void Organizarnota(int[] vetor) {
		Arrays.sort(vetor);
		System.out.println("Organizando por ordem decrescente");
		for (int i = 9; i > 0; i--) {
			System.out.println("Nota:" + " " + vetor[i]);
		}
	}

	public static int[] Obternota(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Digite uma nota: ");
			vetor[i] = sc.nextInt();
			if(vetor[i] > 10) { 
				System.out.println("Nota invalida! Nota máxima permitida: 10");
				vetor[i] = 10;
			}
		}
		System.out.println("Deseja inserir outras notas?");
		String resposta = sc.next();
		if(resposta.equals("S") || resposta.equals("s")) { 
			 Obternota(vetor);
		} else { 
			Organizarnota(vetor);
		}
		return vetor;
	}

	

}
