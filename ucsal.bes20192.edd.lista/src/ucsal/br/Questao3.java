/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

import java.util.Scanner;
/*
    Escreva um programa em Java que simule uma calculadora simples, usando o
    console Java como único disposiƟvo de entrada e saída. Para tanto, cada entrada para a
    calculadora, seja um número, como 12,34 ou 1034, ou um operador como +, -, /, * ou
    =, pode ser digitada em linhas separadas. Depois de cada entrada, deve-se exibir no
c   onsole Java o que seria apresentado na calculadora.
 */
public class Questao3 {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        oprchoice(); 
    }

    public static void oprchoice() {
        System.out.println("Bem-vindo a calculadora!");
        System.out.println("Operações disponíveis: ");
        System.out.println("(1) soma");
        System.out.println("(2) subtração");
        System.out.println("(3) multiplicação");
        System.out.println("(4) divisão");
        System.out.print("Por favor escolha sua operação: ");
        int opr = sc.nextInt();
        switch(opr) { 
            case 1: 
                opsoma();
                break;
            case 2:
                opsub();
                break;
            case 3: 
                opmult();
                break;
            case 4:
                opdiv();
                break;
            default:
                System.out.println("Operação invalida!");
                break;
        }
    }

    public static void opsoma() {
        System.out.println("Por favor digite um número: ");
        int num1 = sc.nextInt();
        System.out.println("Por favor digite um número: ");
        int num2 = sc.nextInt();
        int res = num1 + num2;
        System.out.println("Resultado: " + res);
    }

    public static void opsub() {
        System.out.println("Por favor digite um número: ");
        int num1 = sc.nextInt();
        System.out.println("Por favor digite um número: ");
        int num2 = sc.nextInt();
        int res = num1 - num2;
        System.out.println("Resultado: " + res);
    }

    public static void opmult() {
        System.out.println("Por favor digite um número: ");
        int num1 = sc.nextInt();
        System.out.println("Por favor digite um número: ");
        int num2 = sc.nextInt();
        int res = num1 * num2;
        System.out.println("Resultado: " + res);
    }

    public static void opdiv() {
        System.out.println("Por favor digite um número: ");
        int num1 = sc.nextInt();
        System.out.println("Por favor digite um número: ");
        int num2 = sc.nextInt();
        int res = num1 / num2;
        System.out.println("Resultado: " + res);
    }
}
