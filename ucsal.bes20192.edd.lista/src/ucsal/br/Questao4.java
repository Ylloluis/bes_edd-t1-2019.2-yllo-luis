/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

import javax.swing.*;
/**
 * 4º) Escreva um programa que leia a população mundial atual e a taxa de crescimento
demográfica anual e exiba a população esƟmada depois de um, dois, três, quatro e
cinco anos.
 */
public class Questao4 {
    public static void main(String[] args) {
        setprop();
    }

    private static int setprop() {
        int prop = 0;
        try { 
           prop = Integer.parseInt(JOptionPane.showInputDialog(null, "Por favor digite a proporção de crescimento", "Programa", JOptionPane.PLAIN_MESSAGE));
           if(prop > 15) { 
               JOptionPane.showMessageDialog(null, "Proporção invalida", "Programa", JOptionPane.ERROR_MESSAGE);
               setprop();
           } else { 
               setpop(prop);
           }
        } catch(Exception e) { 
            JOptionPane.showMessageDialog(null, "Proporção invalida", "Programa", JOptionPane.ERROR_MESSAGE);
            setprop();
        }
        
        return prop;
    }

    public static int setpop(int prop) {
        int pop = 0;
        try { 
           pop = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o numero da população", "Programa", JOptionPane.PLAIN_MESSAGE));
           if(pop > 90000) { 
               JOptionPane.showMessageDialog(null, "População invalida", "Programa", JOptionPane.ERROR_MESSAGE);
               setpop(prop);
           } else { 
               calcpop(prop, pop);
           }
        } catch(Exception e) { 
            JOptionPane.showMessageDialog(null, "Proporção invalida", "Programa", JOptionPane.ERROR_MESSAGE);
            setprop();
        }
        
        
        return pop;
    }

    public static void calcpop(int prop, int pop) {
        int res = 0;
        for (int i = 1; i < 6; i++) {
            res += prop * pop;
            JOptionPane.showMessageDialog(null, "Resultado proporção por " + i + "\nResultado: " + res, "Programa", JOptionPane.PLAIN_MESSAGE);
        }
        int conf = JOptionPane.showConfirmDialog(null, "Deseja calcular outro crescimento populacional?", "Programa", JOptionPane.INFORMATION_MESSAGE);
        
        if(conf == JOptionPane.YES_OPTION) {
            setprop();
        } else {
            System.exit(conf);
        }
    }


}
