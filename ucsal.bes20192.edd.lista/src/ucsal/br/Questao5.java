/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;

/**
5º) Escreva um programa que detecte em um array de 10 valores int se existe um par
de números no array cujo produto seja par.
 */
public class Questao5 {
    public static void main(String[] args) {
        criarvetor();
    }

    public static int[] criarvetor() {
        int[] vet = new int[10];
        int acum = 2;
        for (int i = 0; i < 10; i++) {
            vet[i] += acum;
            acum += 2;
        }
        splitvet(vet);
        return vet;
    }

    public static void splitvet(int[] vet) {
        for (int i = 0; i < vet.length; i++) {
            if(vet[i] % 2 == 0) { 
                System.out.print(vet[i] + " ");
            }
        }
    }  
}
