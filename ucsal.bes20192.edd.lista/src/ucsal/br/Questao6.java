/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;
import java.util.Scanner;

/**
 *
 * @author ylloluis
 */
public class Questao6 {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        obteren();
    }
    
    
    public static String obteren() {
        String word = "";
        System.out.print("Digite uma palavra: ");
        word = sc.nextLine();
        invertword(word);
        return word;
    }

    public static void invertword(String word) {
        String reverse = new StringBuilder(word).reverse().toString();
        System.out.println("Palavra invertida: " + reverse);
    }
    
    
}
