/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucsal.br;
import java.util.Scanner;
/**
 *
 * @author ylloluis
 */
public class Questao9 {
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        obternumero();
    }
    
    public static int obternumero() {
        System.out.print("Por favor digite um número: ");
        int num = sc.nextInt();
        
        calcraiz(num);
        return num;
    }

    public static void calcraiz(int num) {
        int acum = 1;
        int cont = 0;
        while(num > 0) {
            num = num / acum;
            acum += 2;
            cont++;
        }
        System.out.println("Raiz: " + cont);
    }
}
